package com.pragmaticcoders.solution;

import com.pragmaticcoders.solution.domain.TopProductCsvGenerator;

import java.io.IOException;

public class Main
{
    public static void main(String[] args) throws IOException
    {
        new TopProductCsvGenerator().generate();
    }


}
