package com.pragmaticcoders.solution.utils;

import java.io.InputStream;
import java.util.Optional;

public class FilesUtil
{
    public InputStream createInputStreamToResource(String resourceFile)
    {
        return Optional.ofNullable(getClass())
                .map(Class::getClassLoader)
                .map(classLoader -> classLoader.getResourceAsStream(resourceFile))
                .orElseThrow(() -> new RuntimeException("Could not create path to resource: " + resourceFile));
    }
}
