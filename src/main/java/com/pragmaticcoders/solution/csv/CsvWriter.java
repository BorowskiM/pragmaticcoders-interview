package com.pragmaticcoders.solution.csv;

import net.sf.jsefa.Serializer;
import net.sf.jsefa.csv.CsvIOFactory;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class CsvWriter
{
    public <T> void  write(List<T> inputs, String fileName)
    {
        Serializer serializer = CsvIOFactory.createFactory(Product.class).createSerializer();
        BufferedWriter writer = createBufferedWriter(fileName);
        serializer.open(writer);
        inputs.forEach(serializer::write);

        serializer.close(true);
    }

    private BufferedWriter createBufferedWriter(String fileName)
    {
        try
        {
            return new BufferedWriter(new FileWriter(fileName));
        } catch (IOException e)
        {
            throw new RuntimeException("Could not create file: " + fileName);
        }
    }
}
