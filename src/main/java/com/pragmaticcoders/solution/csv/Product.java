package com.pragmaticcoders.solution.csv;

import com.pragmaticcoders.solution.csv.converters.DoubleConverter;
import net.sf.jsefa.csv.annotation.CsvDataType;
import net.sf.jsefa.csv.annotation.CsvField;

@CsvDataType()
public class Product
{
    @CsvField(pos = 1)
    private Integer id;

    @CsvField(pos = 2, converterType = DoubleConverter.class)
    private Double price;

    @CsvField(pos = 3)
    private String currencyCode;

    @CsvField(pos = 4)
    private Integer quantity;

    @CsvField(pos = 5)
    private Integer matchingId;

    public Integer getId()
    {
        return id;
    }

    public Double getPrice()
    {
        return price;
    }

    public String getCurrencyCode()
    {
        return currencyCode;
    }

    public Integer getQuantity()
    {
        return quantity;
    }

    public Integer getMatchingId()
    {
        return matchingId;
    }
}
