package com.pragmaticcoders.solution.csv;

import com.pragmaticcoders.solution.csv.converters.DoubleConverter;
import net.sf.jsefa.csv.annotation.CsvDataType;
import net.sf.jsefa.csv.annotation.CsvField;

@CsvDataType()
public class Currency
{
    @CsvField(pos = 1)
    private String currency;

    @CsvField(pos = 2, converterType = DoubleConverter.class)
    private Double ratio;

    public String getCurrency()
    {
        return currency;
    }

    public Double getRatio()
    {
        return ratio;
    }
}
