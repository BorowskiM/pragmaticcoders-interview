package com.pragmaticcoders.solution.csv;

import net.sf.jsefa.csv.annotation.CsvDataType;
import net.sf.jsefa.csv.annotation.CsvField;

@CsvDataType()
public class Match
{
    @CsvField(pos = 1)
    private Integer matchingId;

    @CsvField(pos = 2)
    private Integer topPricedCount;

    public Integer getMatchingId()
    {
        return matchingId;
    }

    public Integer getTopPricedCount()
    {
        return topPricedCount;
    }
}
