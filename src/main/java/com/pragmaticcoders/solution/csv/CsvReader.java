package com.pragmaticcoders.solution.csv;

import net.sf.jsefa.Deserializer;
import net.sf.jsefa.common.lowlevel.filter.HeaderAndFooterFilter;
import net.sf.jsefa.csv.CsvIOFactory;
import net.sf.jsefa.csv.config.CsvConfiguration;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CsvReader
{
    public <T> List<T> read(InputStream filePath, Class<T> clazz)
    {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(filePath)))
        {
            CsvConfiguration config = getCsvConfiguration();
            Deserializer deserializer = CsvIOFactory.createFactory(config, clazz).createDeserializer();
            deserializer.open(reader);
            List<T> output = getOutput(deserializer);
            deserializer.close(true);

            return output;
        } catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    private <T> List<T> getOutput(Deserializer deserializer)
    {
        List<T> output = new ArrayList<>();
        while (deserializer.hasNext())
        {
            output.add(deserializer.next());
        }
        return output;
    }

    private CsvConfiguration getCsvConfiguration()
    {
        CsvConfiguration config = new CsvConfiguration();
        config.setFieldDelimiter(',');
        config.setLineFilter(new HeaderAndFooterFilter(1, false, false));
        return config;
    }
}
