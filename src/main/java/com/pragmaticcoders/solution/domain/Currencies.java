package com.pragmaticcoders.solution.domain;

import java.util.Map;
import java.util.Optional;

public class Currencies
{
    private final Map<String, Double> currencies;

    public Currencies(Map<String, Double> currencies)
    {
        this.currencies = currencies;
    }

    public Double retrieveRatio(String currencyCode)
    {
        return Optional.ofNullable(currencies.get(currencyCode))
                .orElseThrow(() -> new RuntimeException("Can not retrieve currency ration for: " + currencyCode));
    }
}
