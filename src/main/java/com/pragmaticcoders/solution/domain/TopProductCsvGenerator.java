package com.pragmaticcoders.solution.domain;

import com.pragmaticcoders.solution.csv.*;
import com.pragmaticcoders.solution.utils.FilesUtil;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TopProductCsvGenerator
{
    public void generate()
    {
        CsvReader csvReader = new CsvReader();
        FilesUtil filesUtil = new FilesUtil();

        Matches matches = new Matches(csvReader.read(filesUtil.createInputStreamToResource("matchings.csv"), Match.class));
        Products products = new Products(csvReader.read(filesUtil.createInputStreamToResource("data.csv"), Product.class));
        Currencies currencies = new Currencies(convertToMap(csvReader, filesUtil));

        List<Product> productsWithMaxTopPricedCount = products.getProductsWithMaxTopPricedCount(matches.getMatchingIdWithMaxTopPricedCount());

        List<Product> topProducts = new TopProductsRetriever().retrieve(currencies, productsWithMaxTopPricedCount);

        new CsvWriter().write(topProducts, "top_products.csv");

    }

    private Map<String, Double> convertToMap(CsvReader csvReader, FilesUtil filesUtil)
    {
        return csvReader.read(filesUtil.createInputStreamToResource("currencies.csv"), Currency.class)
                .stream().collect(Collectors.toMap(Currency::getCurrency,Currency::getRatio));
    }
}
