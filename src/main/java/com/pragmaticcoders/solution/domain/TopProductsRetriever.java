package com.pragmaticcoders.solution.domain;

import com.pragmaticcoders.solution.csv.Product;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TopProductsRetriever
{
    public List<Product> retrieve(Currencies currencies, List<Product> productsWithMaxTopPricedCount)
    {
        double maxTotalPrice = getMaxTotalProductPrice(currencies, productsWithMaxTopPricedCount);

        return productsWithMaxTopPricedCount.stream()
                .filter(product -> hasMaxTotalPrice(currencies.retrieveRatio(product.getCurrencyCode()), maxTotalPrice, product))
                .collect(Collectors.toList());
    }

    private  double getMaxTotalProductPrice(Currencies currencies, List<Product> productsWithMaxPriced)
    {
        final Comparator<Product> highestTotalPriceComparator =
                (p1, p2) -> Double.compare(getTotalPrice(currencies.retrieveRatio(p1.getCurrencyCode()), p1),
                        getTotalPrice(currencies.retrieveRatio(p2.getCurrencyCode()), p2));

        return  productsWithMaxPriced.stream()
                .max(highestTotalPriceComparator)
                .map(productWithHighestTotalPrice -> getTotalPrice(currencies.retrieveRatio(productWithHighestTotalPrice.getCurrencyCode()), productWithHighestTotalPrice))
                .orElse(0.0);
    }

    private  boolean hasMaxTotalPrice(Double currencyRatio, double maxTotalPrice, Product product)
    {
        return getTotalPrice(currencyRatio, product) == maxTotalPrice;
    }

    private  double getTotalPrice(Double currencyRatio, Product product)
    {
        return product.getQuantity() * product.getPrice() * currencyRatio;
    }
}
