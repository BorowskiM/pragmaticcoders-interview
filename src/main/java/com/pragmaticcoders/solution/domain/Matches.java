package com.pragmaticcoders.solution.domain;

import com.pragmaticcoders.solution.csv.Match;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Matches
{
    private final List<Match> matches;

    public Matches(List<Match> matches)
    {
        this.matches = matches;
    }

    public List<Integer> getMatchingIdWithMaxTopPricedCount()
    {
        Integer maxPricedCount = getMaxPricedCount();
        return matches.stream()
                .filter(match -> match.getTopPricedCount().equals(maxPricedCount))
                .map(Match::getMatchingId)
                .collect(Collectors.toList());
    }

    private Integer getMaxPricedCount()
    {
        final Comparator<Match> topPricedCountComparator =
                (m1, m2) -> Integer.compare(m1.getTopPricedCount(), m2.getTopPricedCount());
        return matches.stream()
                .max(topPricedCountComparator)
                .map(Match::getTopPricedCount)
                .orElseThrow(() -> new RuntimeException("No matches"));
    }
}
