package com.pragmaticcoders.solution.domain;

import com.pragmaticcoders.solution.csv.Product;

import java.util.List;
import java.util.stream.Collectors;

public class Products
{
    private final List<Product> products;

    public Products(List<Product> products)
    {
        this.products = products;
    }

   public List<Product> getProductsWithMaxTopPricedCount(List<Integer> matchingIdWithMaxTopPriced)
   {
       return products.stream()
               .filter(product -> isProductWithMaxPricedCount(matchingIdWithMaxTopPriced, product))
               .collect(Collectors.toList());
   }

    private boolean isProductWithMaxPricedCount(List<Integer> matchingIdWithMaxPriced, Product product)
    {
        return matchingIdWithMaxPriced.stream()
                .filter(matchingId -> matchingId.equals(product.getMatchingId()))
                .findAny()
                .isPresent();
    }
}
